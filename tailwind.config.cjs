/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx}"],
  mode: "jit",
  theme: {
    extend: {
      colors: {
        primary: "#0D3B66",
        secondary: "#F2EFE9",
        tertiary: "#4777A4",
        "black-100": "#041D35", 
        "black-200": "#041D35", 
        "white-100": "#F2EFE9", 
      },
      boxShadow: {
        card: "0px 35px 120px -15px #041D35",
      },
      screens: {
        xs: "450px",
      },
    },
  },
};
