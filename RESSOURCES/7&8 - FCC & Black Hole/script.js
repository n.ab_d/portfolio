// Redémarrer le jeu
function restartGame() {
    location.reload(); 
}

// Vérifier la collision entre deux éléments
function checkCollision(element1, element2) {
    const rect1 = element1.getBoundingClientRect();
    const rect2 = element2.getBoundingClientRect();

    return (
        rect1.x < rect2.x + rect2.width &&
        rect1.x + rect1.width > rect2.x &&
        rect1.y < rect2.y + rect2.height &&
        rect1.y + rect1.height > rect2.y
    );
}

// Générer un nombre aléatoire entre min et max
function getRandomNumber(min, max) {
    return Math.random() * (max - min) + min;
}

// Créer le joueur
const playerSquare = document.createElement("div");
playerSquare.style.width = "25px";
playerSquare.style.height = "25px";
playerSquare.style.backgroundColor = "black";

playerSquare.style.position = "absolute";


let reduction = 0;
let reducFactor = 0;
let calmer = 0;

window.addEventListener('mousemove', function(e){

    const xPos = e.clientX - playerSquare.clientWidth / 2; 
    const yPos = e.clientY - playerSquare.clientHeight / 2; 
    let playerSize = parseInt(playerSquare.style.width);

    playerSquare.style.left = `${xPos}px`;
    playerSquare.style.top = `${yPos}px`;

    // Réduire la taille de notre carré lorsqu'il se déplace

    let positionTable = [xPos, yPos]
    if(xPos != positionTable[xPos]){
        if(calmer % 10 == 0){
            playerSquare.style.width = `${playerSize - reduction}px`;
            playerSquare.style.height = playerSquare.style.width;
            console.log('Réduction égale à ' + reduction*reducFactor)
            console.log(playerSquare)
    
            reduction = reduction*reducFactor + 0.000002;
    
            reducFactor = playerSize * 0.028;
            if(reduction > 10){
                reduction = 5;
            }
        }
        
        calmer++
    }

    // Vérifier la collision avec chaque ennemi
    const enemies = document.querySelectorAll('.enemy');
    enemies.forEach(enemy => {
        if (checkCollision(playerSquare, enemy)) {
            // Collision détectée !
            const enemySize = parseInt(enemy.style.width);

            if (playerSize >= enemySize) {
                playerSquare.style.width = `${playerSize + enemySize/2}px`;
                playerSquare.style.height = playerSquare.style.width;
                enemy.remove();
            } else {
                restartGame();
            }
        }
    });
});

document.body.appendChild(playerSquare);

// Ici je fais apparaître des ennemis petits
let randomAmountOfEnemies = getRandomNumber(100, 120);

for (let i = 0; i < randomAmountOfEnemies; i++) {
    const htmlEnemy = document.createElement("div");
    htmlEnemy.classList.add("enemy");

    let bblsize = Math.floor(Math.random() * 100) + 1;
    htmlEnemy.style.width = `${bblsize}px`;
    htmlEnemy.style.height = `${bblsize}px`;

    htmlEnemy.style.position = "absolute";
    htmlEnemy.style.backgroundColor = `rgb(${getRandomNumber(0, 255)}, ${getRandomNumber(0, 255)}, ${getRandomNumber(0, 255)})`
    htmlEnemy.style.top = `${getRandomNumber(0, 1000)}px`;
    htmlEnemy.style.left = `${getRandomNumber(0, 1750)}px`;

    let htmlEnemyTxt = document.createElement("span");
    htmlEnemyTxt.textContent = bblsize;
    htmlEnemy.appendChild(htmlEnemyTxt);

    document.body.appendChild(htmlEnemy);
}

// Ici je fais apparaître des ennemis gros
let randomAmountOfBigEnemies = getRandomNumber(8, 14);

for (let i = 0; i < randomAmountOfBigEnemies; i++) {
    const htmlEnemy = document.createElement("div");
    htmlEnemy.classList.add("enemy");

    let bblsize = Math.floor(Math.random() * 200) + 100;
    htmlEnemy.style.width = `${bblsize}px`;
    htmlEnemy.style.height = `${bblsize}px`;

    htmlEnemy.style.position = "absolute";
    htmlEnemy.style.backgroundColor = `rgb(${getRandomNumber(0, 255)}, ${getRandomNumber(0, 255)}, ${getRandomNumber(0, 255)})`
    htmlEnemy.style.top = `${getRandomNumber(0, 1000)}px`;
    htmlEnemy.style.left = `${getRandomNumber(0, 1750)}px`;

    let htmlEnemyTxt = document.createElement("span");
    htmlEnemyTxt.textContent = bblsize;
    htmlEnemy.appendChild(htmlEnemyTxt);

    document.body.appendChild(htmlEnemy);
}