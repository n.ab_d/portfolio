import {
  javascript,
  typescript,
  html,
  css,
  reactjs,
  tailwind,
  nodejs,
  git,
  fcc, mailingvox
} from "../assets";

import i18n from '../i18n'

export const navLinks = [
  {
    id: "about",
    title: i18n.t('about')
  },
  {
    id: "work",
    title: i18n.t('work')
  },
  {
    id: "contact",
    title: i18n.t('contact')
  },
];

const monbts = [
  {
    title: i18n.t('tablSynth'),
    url:"https://gitlab.com/djadja1/DJADJA/-/blob/main/TablSynth.pdf?ref_type=heads",
  },
  {
    title: i18n.t('sommaireStage'),
    url:"https://gitlab.com/djadja1/DJADJA/-/blob/main/Stage%20BTS%20SIO%202/sommaire.pdf?ref_type=heads",
  },
  {
    title: i18n.t('veilleIA'),
    url:"https://gitlab.com/djadja1/DJADJA",
  },
  {
    title: i18n.t('justifTableau'),
    url:"https://gitlab.com/djadja1/DJADJA/-/tree/main/justifTableau/RESSOURCES?ref_type=heads",
  },
];

const technologies = [
  {
    name: "HTML 5",
    icon: html,
    learningLink:'https://www.freecodecamp.org/learn/2022/responsive-web-design/'
  },
  {
    name: "CSS 3",
    icon: css,
    learningLink:"https://www.freecodecamp.org/learn/2022/responsive-web-design/"
  },
  {
    name: "JavaScript",
    icon: javascript,
    learningLink:'https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures-v8/'
  },
  {
    name: "TypeScript",
    icon: typescript,
    learningLink: 'https://www.youtube.com/watch?v=a02JAryRPVU'
  },
  {
    name: "React JS",
    icon: reactjs,
    learningLink:'https://www.freecodecamp.org/learn/front-end-development-libraries/'
  },
  {
    name: "Tailwind CSS",
    icon: tailwind,
    learningLink:"https://www.youtube.com/watch?v=lCxcTsOHrjo"
  },
  {
    name: "Node JS",
    icon: nodejs,
    learningLink:'https://www.youtube.com/watch?v=bunBbhY4da4'
  },
  {
    name: "git",
    icon: git,
    learningLink:"https://gitlab.com/"
  },

];

const experiences = [
  {
    title: i18n.t('exp1title'),
    company_name: ".",
    icon: fcc,
    iconBg: "white",
    date: i18n.t('exp1date'),
    points: [
      i18n.t('exp1P1'),i18n.t('exp1P2'),i18n.t('exp1P3'),i18n.t('exp1P4')],
  },
  {
    title: i18n.t('exp2title'),
    company_name: ".",
    icon: mailingvox,
    iconBg: "white",
    date: i18n.t('exp2date'),
    points: [
      i18n.t('exp2P1'),i18n.t('exp2P2'),i18n.t('exp2P3'),
      ],
  },
];

const projects = [
  {
    name: i18n.t('nameProj1'),
    description:
      i18n.t('descProj1'),
    tags: [
      {
        name: "reactNative",
        color: "white",
      },
      {
        name: "tailwind",
        color: "white",
      },
    ],
    image:'src/assets/projects/voiceChanger2.PNG',
    source_code_link: "https://github.com/nouricatama/voicechanger2",
  },
  {
    name: i18n.t('nameProj2'),
    description:
      i18n.t('descProj2'),
    tags: [
      {
        name: "java",
        color: "white",
      },
      {
        name: "javaFX",
        color: "white",
      },
    ],
    image:'src/assets/projects/trainDesMers.png',
    source_code_link: "https://gitlab.com/Lotfi-A/train-des-mers",
  },
  {
    name: i18n.t('nameProj3'),
    description:
      i18n.t('descProj3'),
    tags: [
      {
        name: "reactDOM",
        color: "white",
      },
      {
        name: "js",
        color: "white",
      },
    ],
    image:'src/assets/projects/kanajer.png',
    source_code_link: "https://gitlab.com/n.ab_d/kanajer",
  },
]; export { monbts, technologies, experiences, projects };