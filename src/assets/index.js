import close from "./close.svg";
import menu from "./menu.svg";
import github from "./github.png";
import fcc from './company/fcc.png'
import mailingvox from "./company/mailingvox.png"

import css from "./tech/css.png";
import git from "./tech/git.png";
import html from "./tech/html.png";
import javascript from "./tech/javascript.png";
import nodejs from "./tech/nodejs.png";
import reactjs from "./tech/reactjs.png";
import tailwind from "./tech/tailwind.png";
import typescript from "./tech/typescript.png";


export {
  close,
  css,
  git,
  html,
  javascript,
  nodejs,
  reactjs,
  tailwind,
  typescript,
  menu,
  github,
  fcc,
  mailingvox
};
