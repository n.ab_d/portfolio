import React from "react";
import Tilt from "react-tilt";
import { motion } from "framer-motion";

import { styles } from "../styles";
import { monbts } from "../constants";
import { SectionWrapper } from "../hoc";
import { fadeIn, textVariant } from "../utils/motion";
import i18next from "i18next";



const ServiceCard = ({ index, title, url }) => (
  <Tilt className='xs:w-[250px] w-full'>
    <motion.div
      variants={fadeIn("right", "spring", index * 0.5, 0.75)}
      className='w-full green-pink-gradient p-[1px] rounded-[20px] shadow-card'
    >
      <div onClick={() => window.open(url, '_blank')}
        options={{
          max: 45,
          scale: 1,
          speed: 450,
        }}
        className='bg-tertiary rounded-[20px] py-5 px-12 min-h-[280px] flex justify-evenly items-center flex-col'
      >
        <h3 className='text-white text-[20px] font-bold text-center'>
          {title}
        </h3>
      </div>
    </motion.div>
  </Tilt>
);

const About = () => {
  return (
    <>
      <motion.div variants={textVariant()}>
        <p className={styles.sectionSubText}>{i18next.t('intro')}</p>
        <h2 className={styles.sectionHeadText}>{i18next.t('myBTS')}</h2>
      </motion.div>

      <motion.p
        variants={fadeIn("", "", 0.1, 1)}
        className='mt-4 text-secondary text-[17px] max-w-3xl leading-[30px]'
      >
        
      </motion.p>

      <div className='mt-20 flex flex-wrap gap-10'>
        {monbts.map((bull, index) => (
          <ServiceCard url={bull.url} key={bull.title} index={index} {...bull} />
        ))}
      </div>
    </>
  );
};

export default SectionWrapper(About, "about");
