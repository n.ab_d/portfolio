import React, { Suspense, useEffect, useState } from "react";
import { Canvas } from "@react-three/fiber";
import { Preload, useFBX } from "@react-three/drei";

import CanvasLoader from "../Loader";

const Computers = () => {
  const computer = useFBX("./desktop_pc/scene.fbx");

  return (
    <mesh>
      <spotLight
        position={[10,25, -25]}
        angle={2}
        penumbra={0}
        intensity={0.5}
        castShadow
      />
      <pointLight intensity={1} />
      <primitive
        object={computer}
        scale={0.07}
        position={[-2, -10.5, -1]}
        rotation={[0.0, 1.9, 0]}
      />
    </mesh>
  );
};

const HouseCanva = () => {
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const mediaQuery = window.matchMedia("(max-width: 500px)");

    setIsMobile(mediaQuery.matches);

    const handleMediaQueryChange = (event) => {
      setIsMobile(event.matches);
    };

    // Add the callback function as a listener for changes to the media query
    mediaQuery.addEventListener("change", handleMediaQueryChange);

    // Remove the listener when the component is unmounted
    return () => {
      mediaQuery.removeEventListener("change", handleMediaQueryChange);
    };
  }, []);

  return (
    <Canvas
      frameloop='demand'
      shadows
      dpr={[1, 2]}
      camera={{ position: [20, 3, 5], fov: 95 }}
      gl={{ preserveDrawingBuffer: true }}
    >
      <Suspense fallback={<CanvasLoader />}>
        <Computers isMobile={isMobile} />
      </Suspense>

      <Preload all />
    </Canvas>
  );
};

export default HouseCanva;
